/*
 * Facebook Group Manager
 *
 * Copyright 2015, Mathieu Mescam
 * Author: Mathieu Mescam <mathieu@mesc.am>
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();

var lib = require('./lib');


// Serve static files in public/    
app.use(express.static('public'));


// Start server
var server = app.listen(3000, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('Server listening at http://%s:%s', host, port);
  console.log('dirname: ' + __dirname);

});