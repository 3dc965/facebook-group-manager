/*
 * Facebook Group Manager
 *
 * Copyright 2015, Mathieu Mescam
 * Author: Mathieu Mescam <mathieu@mesc.am>
 */

module.exports.saveFile = function(req, res) {

	var files = req.files;
	console.log(req.files);
	console.log(req.body);

	var status = 200;
	var body = {
		'status': status,
		'content': 'uploaded: ' + files
	};
	
	res.status(status).send(body);
};

module.exports.getInsights = function(req, res) {
	
	var status = 200;
	var body = {
		'status' : status,
		'content' : {'type' : 'empty'}
	};

	res.status(status).send(body);
};