# Facebook Group Manager

This app aims to visualize the activity of a Facebook Group.

## Table of contents

- [Quick start](#quick-start)
- [Features](#features)
- [Documentation](#documentation)
- [Copyright and license](#copyright-and-license)

## Quick Start

### Pre-requisites

## Server

* [Git](http://git-scm.com/)
* [Node.JS](https://nodejs.org)
* [Bower](http://bower.io/)

## Client

* [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)

### Installation

Clone the repo

	git clone https://bitbucket.org/msecam/facebook-group-manager
	
Go to  `facebook-group-manager`

	cd facebook-group-manager

Install  dependencies

	[sudo] npm install

	bower install [--allow-root]
	
### Usage

Start the server

	node app

Open your browser

	http://localhost:3000

Drag n Drop an XLSX. Sample files available in 'datasets'.

Et voilà.

## Features

### Front end

#### File upload
- [x] Drag n Drop
- [x] Multiple File Handling

#### XLSX parsing
- [x] Count users
- [x] Count posts
- [x] Count days
- [x] Count posts per users
- [x] Count posts per days
- [x] Count posts per service

#### Export
- [] Save Insights
- [] Save URLS
- [] Call Youtube / Soundcloud / ...  API

#### User interface
- [] Upload view 
- [] Insight view
- [] Export view
- [] Navigation

### Back end

- [x] Static server
- []

## Documentation

### Mockups

* [mockups](./mockups)

### CSV Parsing

* [Google `bash csv`](http://stackoverflow.com/questions/1560393/bash-shell-scripting-csv-parsing)
* [Google `multitrhreaded csv parser`](http://www.coderanch.com/t/541427/threads/java/Structuring-Multi-Threaded-Application-Parse)

### Javascript / Web API
* [File API](http://www.w3.org/TR/FileAPI/)
* [hashchange - Event reference | MDN](https://developer.mozilla.org/en-US/docs/Web/Events/hashchange)
* [Document.plugins - Web API Interfaces | MDN](https://developer.mozilla.org/en-US/docs/Web/API/Document/plugins)
* [HTML DOM createElement() Method](http://www.w3schools.com/jsref/met_document_createelement.asp)

### Dependencies
* [xlsx](https://www.npmjs.com/package/xlsx)

### Node.js / Express 

* [Express 4.x - API Reference](http://expressjs.com/api.html)
* [node.js - static files with express.js - Stack Overflow](http://stackoverflow.com/questions/10434001/static-files-with-express-js)
* [connect/static.js at 2.3.3 · senchalabs/connect](https://github.com/senchalabs/connect/blob/2.3.3/lib/middleware/static.js#L140)
* [How to serve static files in Express for Node.js Code Example - Runnable](http://runnable.com/UWw3g0PKxoAWAA6K/how-to-serve-static-files-in-express-for-node-js)
* [Output: How to serve static files in Express for Node.js](http://runnable.com/VTGQCtnGwCBVyqBJ/output)