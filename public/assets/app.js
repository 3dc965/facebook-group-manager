/*
 * Facebook Group Manager
 *
 * Copyright 2015, Mathieu Mescam
 * Author: Mathieu Mescam <mathieu@mesc.am>
 */

 // Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {
  // Great success! All the File APIs are supported.
} else {
  console.log('The File APIs are not fully supported in this browser.');
}

 // TODO : Refactor !
function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    var files = evt.dataTransfer.files; // FileList object.

    // files is a FileList of File objects. List some properties.
    var outputList = [];
    for (var i = 0, f; f = files[i]; i++) {

      // Only process xlsx files.
      if (!f.type.match('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')) {
        console.error('File type error: %s type is %s', f.name, f.type);
        continue;
      }


      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(file) {
        return function(e) {
          var data = e.target.result;
          // console.dir(data);

          var workbook = XLSX.read(data, {type: 'binary'});
          var worksheet = workbook.Sheets[workbook.SheetNames[0]];
          // console.log(worksheet);

          var sheetToJson = XLSX.utils.sheet_to_json(worksheet);
          // console.log(sheetToJson);

          var groupData = new FacebookGroupData(sheetToJson);
          console.log(groupData);

          // groupData.debug(groupData);

          // gruoupName + groupPeriod = groupInfo
          var groupName = document.createElement('h1');
              groupName.innerHTML = '<hr>' + groupData._name;

          var groupPeriod = document.createElement('h2');
              groupPeriod.innerHTML = 'From : <i>' +  groupData._monitoringPeriod.from + '</i> To : <i>' + groupData._monitoringPeriod.to + '</i>';

          var groupInfo = document.createElement('div');
              groupInfo.appendChild(groupName);
              groupInfo.appendChild(groupPeriod);

            // groupUsers + groupPosts + groupDays = groupInsight
          var groupUsers = document.createElement('div');
              groupUsers.classList.add('insight');
              groupUsers.innerHTML = '<span>' + groupData.totalUsers + '</span><span>Users</span>' ;

          var groupPosts = document.createElement('div');
              groupPosts.classList.add('insight');
              groupPosts.innerHTML = '<span>' + groupData.totalPosts + '</span><span>Posts</span>';

          var groupDays = document.createElement('div');
              groupDays.classList.add('insight');
              groupDays.innerHTML = '<span>' + groupData.totalDays + '</span><span>Days</span>';

          var groupInsight = document.createElement('div');
              groupInsight.classList.add('insight-container');

              groupInsight.appendChild(groupUsers);
              groupInsight.appendChild(groupPosts);
              groupInsight.appendChild(groupPosts);
              groupInsight.appendChild(groupDays);

          var groupView = document.createElement('div');
              groupView.classList.add('group-container');

            groupView.appendChild(groupInfo);
            groupView.appendChild(groupInsight);

          document.getElementById('content').appendChild(groupView);


          
        };
      })(f);

      reader.readAsBinaryString(f);
    }
    // Remove dropzone when files are uploaded
    // TODO : Add progress bar
    document.getElementById('drop_zone').classList.add('hidden');

    // Displays file info...
    // document.getElementById('list').innerHTML = '<ul>' + outputList.join('') + '</ul>';

  }

  function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  }

  // Setup the dnd listeners.
  var dropZone = document.getElementById('drop_zone');
  dropZone.addEventListener('dragover', handleDragOver, false);
  dropZone.addEventListener('drop', handleFileSelect, false);