/*
 * Facebook Group Manager
 *
 * Copyright 2015, Mathieu Mescam
 * Author: Mathieu Mescam <mathieu@mesc.am>
 */

var FacebookGroupData = function(arr) {
	this._name = arr[0]['Group'];
	this._monitoringPeriod = this.monitoringPeriod(arr);

	this.userPost = this.postPerUser(arr);
	this.dayPost =  this.postPerDay(arr);
	this.servicePost = this.postPerService(arr);

	this.totalUsers = this.objectSize(this.userPost);
	this.totalPosts = this.objectSize(this.postPerURL(arr));
	this.totalDays = this.objectSize(this.dayPost);

}

FacebookGroupData.prototype = {

	/*
	 * Compute the number of posts by a user
	 * @return Object
	 */
	postPerUser: function(arr) {
		return arr.map(function(item) {
			return item.User;
		}).reduce(function(prev, next) {
				prev[next] = (prev[next] + 1) || 1;
				return prev;
			}, {});
	},

	/*
	 * Compute the number of posts that has the same URL to check if an url has already been posted
	 * @return Object
	 */
	postPerURL: function(arr) {
		return arr.map(function(item) {
			return item.URL;
		}).reduce(function(prev, next) {
				prev[next] = (prev[next] + 1) || 1;
				return prev;
			}, {});

	},

	/*
	 * Compute the number of posts in a day
	 * @return Object
	 */
	postPerDay: function(arr) {
		var self = this;

		arr.forEach(function(item) {
			item.Date = self.cropDateString(item.Date);
		});

		return arr.map(function(item) {
			return item.Date;
		}).reduce(function(prev, next) {
			prev[next] = (prev[next] + 1) || 1;
			return prev;
		}, {});


	},

	/*
	 * Compute the amount of posts by service
	 * @return Object
	 */
	postPerService: function(arr) {
		var usage = {};
		var services = {
			'youtube' : [],
			'soundcloud' : [],
			'dailymotion' : [],
			'tco' : [],
			'fbcdn' : []
		};


		var urls = arr.map(function(item) {
			return item.URL;
		});

		// Loop over urls then loop over services to see if service is contained
		urls.forEach(function(url) {

			// console.log(url);
			// Replace special chars to match url like youtu.be
			searchUrl = url.replace(/[^a-zA-Z0-9_-]/g,'');

			Object.keys(services).forEach(function(service) {
				if(searchUrl.indexOf(service) > 0) {
					services[service].push(url);
				}
			usage[service] = services[service].length;
			});
		});

		return usage;
	},

	/*
	 * Get the first and the last date of the set
	 * @return Object
	 */
	monitoringPeriod: function(arr) {
		var self = this;

		var from = self.cropDateString(arr[0]['Date']);
		var to = self.cropDateString(arr[arr.length - 1]['Date']);

		return {'from' : from, 'to' : to};

	},

	/*
	 * Get the size of an Object
	 * @return Number
	 */
	objectSize: function(obj) {
		return Object.keys(obj).length;
	},

	/*
	 * Handle date attribute
	 * @return String
	 */
	 cropDateString: function(string) {
	 	var re = /[a-z]+ [0-9]{2}, [0-9]{4}/i

	 	return re.exec(string).toString();
	 },

	 debug: function(groupData) {
          var dataList = [];
          
          dataList.push('<li>');
          Object.keys(groupData).forEach(function(attr) {
            dataList.push('<pre>', attr, ' : ', groupData[attr],  '</pre>');
          });
          dataList.push('</li>');

          var div = document.createElement('div');
          div.classList.add('insight');
          div.innerHTML = '<ul>' + dataList.join('') + '</ul>';

          document.getElementById('data').appendChild(div);
      }
};

